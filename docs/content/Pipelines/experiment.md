Experiments
====================
Training your Machine Learning models in MLReef is called _Experiments_.
Experiments work similarly to the Data Operation pipelines and the Data Visualization.

You can run multiple experiments and compare the results. 


Ho to create a new Experiment
------------------- 
1. Navigate to your repository, to Experiments and click the button `New Experiment`
2. Click the folder `Data` and select the correct data folder for your experiment
3. Drag a suitable algorithm from the right hand list to the main area in the center.
4. Set all necessary parameters for this model.