MLReef Docs
===================

Welcome to [MLReef](https://mlreef.com) documentation. 

> MLReef is currently on **closed alpha** and therefore, this documentation is subject to change. 

For all early access user we suggest starting with this [**early access documentation**](user/general/README.md). It will give you a fast introduction to the basic concepts and features of MLReef and guide you savely on your pioneering voyage.

> If you need support and you can´t find help here, don´t hesitate to either [raise a ticket](https://gitlab.com/mlreef/frontend/issues), connect with the MLReef community through our [slack channel](https://mlreefcommunity.slack.com) or send us an email at help@mlreef.com. You are never alone in the reef!


## Overview

No matter how you use MLReef, we have the documentation for you: 

| Essential Documentation  | Essential Documentation  |
|---|---|
| [**User Documentation**](user/README.md)<br>Discover features and concepts for MLReef users. | [**Early Access Documentation**](user/general/README.md)<br>Best start for all early access users. |
| [**Contributing to MLReef**](user/contributing.md)<br>Explore this section if you want to contribute.  | [**New to GIT and MLReef?**](#new_to_git)<br>We have the resources to get you started.  |
| [**Use-Cases**](user/general/use_cases.md)<br>Explore the power of MLReef through hands-on use cases!  | [**MLReef releases**](user/releases.md)<br>Whats new in MLReef.  |


## Popular documentation

Take a look at some of our most popular documentation resources:

| Essential Documentation | Documentation for |
|---|---|
| [Parameter settings](user/pipelines/parameters.md)  | Setting parameter values on data operations and models. |
| [SSH Authentification](user/general/ssh/doc_ssh_README.md)  | Secure your network communication. |


## The entire MLOps cycle

MLReef aims to be the first single application for the entire value chain when creating Machine Learning (ML) projects. 
MLReef builds on the experience gathered in [Concurrent DevOps](https://en.wikipedia.org/wiki/DevOps) and ML specific operations that will enable you to work faster, more transparent and use the power of the entire MLReef community to create best Machine Learning. 

MLReef provides solutions for [all the stages in MLOps](../User_Documentation)

The following links provide documentation for each MLOps stage: 

| MLOps stage  | Documentation for  |
|---|---|
| [Manage data](../User_Documentation)  | Data management features. |
| [Manage models and operations](../User_Documentation)  | Source code management features. |
| [Experiment](../User_Documentation)  | Training, experiment metrics and output files. |


### <a name="manage_data"></a> Manage data

MLReef consolidates your data sets in one [distributed version control system](../User_Documentation) that´s easily managed and processed without disrupting your workflow. 

The following documentation relates to the MLOps **Manage data** stage: 

| Manage data topics  | Documentation for  |
|---|---|
| [ML projects](../User_Documentation)  | Creating and managing ML projects. |
| [Data processing](user/Pipelines/data_processing.md)  | Data import, management and pre-processing. |
| [Data visualization](../User_Documentation)  | Built-in data visualization pipeline for plotting and charting. |
| [Version control](../User_Documentation)  | GIT and MLReef based workflow for full version control on your data. |


### <a name="manage_models"></a> Manage models and operations

MLreef is built from ground up to offer the highest degree of collaboration, within your team and throughout the entire MLReef community. The marketplace offers you instant accessible models and operations with full access.

The following documentation relates to the MLOps **Manage models and operations** stage: 

| Manage models and operations topics  | Documentation for  |
|---|---|
| [Pipelines](../User_Documentation)  | Data sourced pipelines for models and operations. |
| [Code repositories](../User_Documentation)  | GIT based repositories for your models and operations. |


### <a name="experiments"></a> Experiment

Training your model on your data 

The following documentation relates to the MLOps **Experiment** stage: 

| Experiment topics  | Documentation for  |
|---|---|
| [Pipeline](../User_Documentation)  | Models, parameters, training and resources. |
| [Reproducibility](../User_Documentation)  | Understanding and reproducing each value-added step. |
| [Model output](../User_Documentation)  | Metrics and model binary storage and download. |


## <a name="new_to_git"></a> New to GIT and MLReef

Working with new systems can be daunting. 

We have the following documentation to get you started with MLReef:

| Topic  | Documentation for  |
|---|---|
| [MLReef basic guide](../User_Documentation)  |  Start working on the command line and with MLReef. |
| [MLReef workflow overview](../User_Documentation)  | Enhance your workflow with the best of MLReef workflow. |


## <a name="user_account"></a> User account

Lear more about user management in MLReef. 

| Topic  | Documentation for  |
|---|---|
| [User account](../User_Documentation)  | 	Manage your account. |
| [Authentication](../User_Documentation)  | Account security with two-factor authentication, set up your SSH keys, and deploy keys for secure access to your projects. |


## <a name="git_mlreef"></a> GIT and MLReef

Learn more about using Git, and using Git with MLReef:

| Topic  | Documentation for  |
|---|---|
| [GIT](../User_Documentation)  | 	Getting started with Git, branching strategies, Git LFS, and advanced use. |
| [GIT cheat sheet](../User_Documentation)  | Download a PDF describing the most used Git operations. |
| [MLReef GIT flow](../User_Documentation)  | Explore the best of Git with the MLReef Flow strategy. |

