export const SET_FILEUPLOAD = 'setFileUpload';
export const SET_CONTENT = 'setContent';
export const SET_TARGET = 'setTarget';
export const SET_MSG = 'setMsg';
export const SET_MR = 'setMR';
export const SET_UPLOADBTN = 'setUploadBtn';
export const SET_PROGRESS = 'setProgress';
export const SET_LOADING = 'setLoading';
