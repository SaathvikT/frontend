### Problem to Solve
> Describe the desired state of the system in the style of:
> “As a (User / Manager / Admin / Guest) I want to be able to do x”


### Technical Solution
> Add technical implementation details and the results of the ticket's discussion here.


### Terminology
[MLReef's Glossary](https://gitlab.com/mlreef/www-mlreef-com/-/tree/master/handbook/glossary.md)



Additional Notes
=====================
